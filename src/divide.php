<?php
/**
 * @param $a
 * @param $b
 *
 * @return float|int
 */
function divide($a, $b)
{
    if (!is_numeric($a) || !is_numeric($b)) {
        throw new InvalidArgumentException('Numbers MUST be numeric');
    }

    if ($b === 0) {
        throw new InvalidArgumentException('Denominator MUST be greater than zero');
    }

    return $a / $b;
}