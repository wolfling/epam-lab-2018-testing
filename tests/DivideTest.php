<?php

namespace tests;

use PHPUnit\Framework\TestCase;

require './src/divide.php';

class DivideTest  extends TestCase
{
    /**
     * @return array
     */
    public function dividePositiveDataProvider()
    {
        return [
            [1, 1, 1],
            [4, 2, 2],
            [4.8, 2, 2.4],
        ];
    }

    /**
     * @return array
     */
    public function divideNegativeDataProvider()
    {
        return [
            [1, 0, 'Denominator MUST be greater than zero'],
            ['test', 1, 'Numbers MUST be numeric'],
            [1, 'test', 'Numbers MUST be numeric'],
        ];
    }

    /**
     * @dataProvider dividePositiveDataProvider
     */
    public function testDividePositive($a, $b, $expected)
    {
        $result = divide($a, $b);

        $this->assertEquals($expected, $result);
    }

    /**
     * @dataProvider divideNegativeDataProvider
     */
    public function testDivideNegative($a, $b, $expected)
    {
        $this->expectExceptionMessage($expected);

        divide($a, $b);
    }
}